%{
#include "sintactico.tab.h"
#include <math.h>
#include <string.h>
#include <stdlib.h>
%}
Cadena \"[^"\n]*\"
LETRA [a-zA-Z]
DIGITO [0-9]
INT "-"?{DIGITO}+
FLOAT {INT}+"."{DIGITO}*
CAD {LETRA}+



%%
{INT}           {      
                yylval.entero = atoi(yytext);
                return (ENTERO);
                }

{FLOAT}         {

                yylval.real=atof(yytext);
                return (REAL);
                }

{Cadena}        {
                int i;
                for(i=1;i<strlen(yytext)-1;i++)
                        yytext[i-1] = yytext[i];                        
                yytext[i-1] = '\0';                
                yylval.cadena=strdup(yytext);
                return (CADENA);
                }  

"-"             {
                return (MENOS);
                }    

"+"	 	{
                return (MAS);
                }

"/"             {
                return (ENTRE);
                }

"*"             {
                return (POR);
                }

"^"             {
                return (POT);
                } 
"%"             {
                return (MOD);
                }
"pow"           {
                yylval.cadena=strdup(yytext);
                return (FPOW);
                }  

"("             {
                return (PA);
                } 
")"             {
                return (PC);
                } 
","             {
                return (COMA);
                } 
";"             {
                return (PTCOMA);
                }           

"\n"            {
                return (yytext[0]);
                }


.               ;
%%
